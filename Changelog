Python Sudoku 0.13 (2008/09/19):
	* Split pysdk.py into several scripts: pysdk.py, pysdk-pdf.py,
	  pysdk-image.py and pysdk-gui.py
	* Simplify the configuration
	* Added -t|--test option to get the difficulty of a sudoku
	* Added --force option to force that the difficulty of the created
	  sudoku is the same as the given difficulty
	* Open .sdk files between systems with different newline

Python Sudoku 0.12.4 (2006/09/23):
	* Update German translation (Mario Bl�ttermann)
	* Fix bug #1560569: give_one not working in GUI

Python Sudoku 0.12.3 (2006/08/04):
	* Fixed Bug #1532235: --use_letters and --numbers_only didn't work if
	  reportlab and PIL weren't installed.
	* Fixed Bug: Show correct usage information depending on the modules
	  present.

Python Sudoku 0.12.2 (2006/07/30):
	* Don't solve the sudoku in GUI while player plays.
	* Don't show semi-solved sudoku in -s|--solve command.

Python Sudoku 0.12.1 (2006/07/02):
	* Use all the algorithms to solve sudokus in GUI.

Python Sudoku 0.12 (2006/06/28):
	* Creation of sudokus with different difficulties (--difficulty):
	  easy, normal or hard
	* Added --use_letters option to use letters for numbers > 9 (default)
          and --numbers_only to use always numbers.
	* Added -m|--modules option to show modules not found
	* Configuration files support
	* Use PIL version < 1.1.5 (Antti Kuntsi (mickut))
	* Catalonian translation (Daniel Fdez Carrodeguas)
	* German translation (Mario Bl�ttermann)
	* Polish translation (hermitt@poczta.onet.pl)
	* Fixed Bug #1373038: Wrong grammer provided by --help option

Python Sudoku 0.11 (2005/12/01):
	* Get format from filename extension in image output (--format=FORMAT
	  to force a format)
	* Added "--print_command" to set the command for print the sudoku
	* Added "Give one number" to Sudoku menu
	* Added "Save as PDF" and "Save as Image" options to gui
	* Fixed some lines not painted in image output
	* Fixed Bug #1370230: Print menu option not working
	* Fixed Bug #1370230: Print not working in Windows

Python Sudoku 0.10 (2005/11/29):
	* More generic grid form support, the board will be Y x X grid of
	  X x Y regions. (Antti Kuntsi (mickut))
	* I18n support
	* Galician translation

Python Sudoku 0.9 (2005/10/24):
	* Use psyco if present (50% faster creation)
	* Convert all classes to new style
	* Added documentation strings

Python Sudoku 0.8 (2005/10/20):
	* Change name to Python Sudoku
	* Fixed typos in howto (Bjarte (the hand))

Python Sudoku 0.7 (2005/10/03):
	* 50% faster creation
	* Redone GUI

Python Sudoku 0.6 (2005/09/29):
	* Modules (reportlab, PIL and pygtk) are checked and if not found
	  the funcionality depending of then are disabled
	* Added --show_fonts to show the available fonts to PDF/Print
	* PDF and print customization (page sizes, fonts, colors)
	* Fix typo: change "color" (USA english) to "colour" (UK english)

Python Sudoku 0.5 (2005/09/26):
	* Creation of Black&White, RGB or RGBA image depending of the colours
	  and transparency
	* Creation of transparent images without background
	* Configurable image size (--width x, --height y)
	* Configurable image font (--font path, --font_size size,
	  --font_colour colour)
	* Configurable image colours (--background colour, --lines colour)
	Note: each colour must be a CSS3-style colour string

Python Sudoku 0.4 (2005/09/19):
	* Save as PDF
	* Save as image (png, jpeg, ... any format supported by PIL)
	* Using a temporal directory in PDF creation for print
	* Changes -i|--info options to -w|--whatis
	* Removed --solved option, instead: solve a sudoku saving it and then
	  print it

Python Sudoku 0.3 (2005/09/15):
	* Improved the creation velocity about 500% faster now (4 secs in my
	  system, still work needed here)
	* Fixed undo/redo in gui
	* Gui can start with a specific sudoku (not obligatory creation at
	  start)

Python Sudoku 0.2 (2005/09/13):
	* Added handicap option for set the difficulty in the sudoku creation
	* Added print support for windows platform, Acrobat Reader needed
	  (feedback needed)
	* Using correct buttons for undo/redo (STOCK_UNDO/REDO instead of
	  STOCK_GO_BACK/FORWARD)
	* Only show filename in pdf if it is set (not None)

Python Sudoku 0.1 (2005/09/12):
	* Creation and solving of sudokus (9x9)
	* Print support in unix (using lpr with a generated pdf using
	  reportlab)
	* Gui interface using gtk
